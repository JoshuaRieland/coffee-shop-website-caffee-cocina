<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8">
	<title>Caffe Cocina Events</title>

	<link rel="stylesheet" type="text/css" href="/assets/css/events.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<script src="/assets/js/JQueryLib.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script type='text/javascript'>
		$(document).ready(function(){
			$('#aboutLink').click(function(){
		  		w = $(window).width();
	  			if(w < 500){
		  			$('#aboutLink').css('margin-bottom','50px');
		  			$('#menuLink').css('margin-bottom','0px');	
		  		}
		  	})
		  	$('#menuLink').click(function(){
		  		w = $(window).width();
	  			if(w < 500){
		  			$('#aboutLink').css('margin-bottom','0px');
		  			$('#menuLink').css('margin-bottom','50px');
		  		}	
		  	})
		})
	</script>
</head>
<body id='body'>



<!-- HEADER/NAVBAR -->
	<div class='col-xs-12 navbarHeader navbar-default'>
		
		<a href='/home'><div class='col-xs-4 col-md-2 navbarLogo' data-toggle='collapse' data-target='#theList'>
		</div></a>

	
		
		<div class='col-xs-6 navbarLink navbarBack collapse navbar-collapse' id='theList'>
			<ul class='centerItem listItems'>
				<li class='navLinks navLinkBack navLinkOff'><a class='navLinkColorOff' href='/home'>Home</a></li>
				
				<li class='dropdown navLinks navLinkBack navLinkOff' id='aboutLink'>
					<a href="#" class='dropdown-toggle listLinkColor' data-toggle='dropdown' role='button' aria-expanded='false'>About</span><span class="caret"></span></a>
					<ul class='dropdown-menu' role='list'>
						<li class='navLinksDropdown '><a class='navLinksDropdownColor' href='/ourstory'>Our Story</a></li>
						<li class='navLinksDropdown '><a class='navLinksDropdownColor' href='/photo_album'>Photos</a></li>
					</ul>
				</li>

				<li class='dropdown navLinks navLinkBack navLinkOff' id='menuLink'>
					<a href="#" class='dropdown-toggle listLinkColor' data-toggle='dropdown' role='button' aria-expanded='false'>Menu<span class="caret"></span></a>
					<ul class='dropdown-menu' role='list'>
						<li class='navLinksDropdown'><a class='navLinksDropdownColor' href='/menu'>Caffe Menu</a></li>
						<li class='navLinksDropdown'><a class='navLinksDropdownColor' href='/catering'>Catering</a></li>
					</ul>
				</li>

				<li class='navLinks navLinkBack navLinkOn'><a class='navLinkColorOn' href='/events'>Events</a></li>
				<li class='navLinks navLinkBack navLinkOff'><a class='navLinkColorOff' href='/contact'>Contact</a></li>

				<li class='navLinks navLinkBack navLinkOff'><a class='navLinkColorOff' href='/review'>Reviews</a></li>
				<li class='navLinks navLinkBack navLinkOff'><a class='navLinkColorOff' href='/merchandise'>Store</a></li>
			</ul>
		</div>
		<button class='pull-right toggleBox collapsed navbar-toggle navbar-collapse' data-toggle='collapse' data-target='#theList'>
        <span class="fa fa-bars fa-lg"></span>
		</button>
	</div>
<!-- END HEADER/NAVBAR -->

<!-- Main Body Content -->

	<!-- Daily Specials -->
    <div class='col-xs-12 col-sm-4 col-sm-offset-4 centerText contentDiv' id='dailySpecialRow'>
      <ul>
        <p class='underline'>  Daily Drink Specials:</p>
        <p class='dailySpecials'>Monday: Mocha Monday</p>
        <p class='dailySpecials'>Tuesday: Dollar Drip Day</p>
        <p class='dailySpecials'>Wednesday: Double Punch Day</p>
        <p class='dailySpecials'>Thursday: Free Extra Shot </p>
        <p class='dailySpecials'>Friday: Free Flavor Day</p>
      </ul>
    </div>
  	<!-- END Daily Specials -->


  	<!-- Event Calendar -->
 	<div class='calendar'>
    	<iframe src="https://www.google.com/calendar/embed?showTitle=0&amp;showCalendars=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;src=caffecocinapoulsbo%40gmail.com&amp;color=%23711616&amp;ctz=America%2FLos_Angeles" style=" border-width:0 " width="800" height="600" frameborder="0" scrolling="no"></iframe>
  	</div>
  	<!-- END Event Calendar -->


<!-- END Main body Content -->



</body>
</html>