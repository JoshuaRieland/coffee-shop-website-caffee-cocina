<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8">
	<title>Caffe Cocina Contacts</title>

	<link rel="stylesheet" type="text/css" href="/assets/css/contact.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<script src="/assets/js/JQueryLib.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script type='text/javascript'>
		$(document).ready(function(){
			$('#aboutLink').click(function(){
		  		w = $(window).width();
	  			if(w < 500){
		  			$('#aboutLink').css('margin-bottom','50px');
		  			$('#menuLink').css('margin-bottom','0px');
		  		}
		  	})
		  	$('#menuLink').click(function(){
		  		w = $(window).width();
	  			if(w < 500){
		  			$('#aboutLink').css('margin-bottom','0px');
		  			$('#menuLink').css('margin-bottom','50px');
		  		}	
		  	})
		})
	</script>

</head>
<body id='body'>



<!-- HEADER/NAVBAR -->
	<div class='col-xs-12 navbarHeader navbar-default'>
		
		<a href='/home'><div class='col-xs-4 col-md-2 navbarLogo' data-toggle='collapse' data-target='#theList'>
		</div></a>

	
		
		<div class='col-xs-6 navbarLink navbarBack collapse navbar-collapse' id='theList'>
			<ul class='centerItem listItems'>
				<li class='navLinks navLinkBack navLinkOff'><a class='navLinkColorOff' href='/home'>Home</a></li>
				
				<li class='dropdown navLinks navLinkBack navLinkOff' id='aboutLink'>
					<a href="#" class='dropdown-toggle listLinkColor' data-toggle='dropdown' role='button' aria-expanded='false'>About</span><span class="caret"></span></a>
					<ul class='dropdown-menu' role='list'>
						<li class='navLinksDropdown '><a class='navLinksDropdownColor' href='/ourstory'>Our Story</a></li>
						<li class='navLinksDropdown '><a class='navLinksDropdownColor' href='/photo_album'>Photos</a></li>
					</ul>
				</li>

				<li class='dropdown navLinks navLinkBack navLinkOff' id='menuLink'>
					<a href="#" class='dropdown-toggle listLinkColor' data-toggle='dropdown' role='button' aria-expanded='false'>Menu<span class="caret"></span></a>
					<ul class='dropdown-menu' role='list'>
						<li class='navLinksDropdown'><a class='navLinksDropdownColor' href='/menu'>Caffe Menu</a></li>
						<li class='navLinksDropdown'><a class='navLinksDropdownColor' href='/catering'>Catering</a></li>
					</ul>
				</li>

				<li class='navLinks navLinkBack navLinkOff'><a class='navLinkColorOff' href='/events'>Events</a></li>
				<li class='navLinks navLinkBack navLinkOn'><a class='navLinkColorOn' href='/contact'>Contact</a></li>

				<li class='navLinks navLinkBack navLinkOff'><a class='navLinkColorOff' href='/review'>Reviews</a></li>
				<li class='navLinks navLinkBack navLinkOff'><a class='navLinkColorOff' href='/merchandise'>Store</a></li>
			</ul>
		</div>
		<button class='pull-right toggleBox collapsed navbar-toggle navbar-collapse' data-toggle='collapse' data-target='#theList'>
        <span class="fa fa-bars fa-lg"></span>
		</button>
	</div>
<!-- END HEADER/NAVBAR -->

<!-- Main Body Content -->
	
		<div class='col-xs-9 col-xs-offset-0 col-sm-6  col-sm-offset-3 col-md-4 col-md-offset-1'>
			<div class='contentDiv contentHeight'>			
				<div class='row'>
					<div class='col-xs-3 cardLabel'>
						<h5>Owners:</h5>
					</div>
					<div class='col-xs-7 cardContent'>
						<h5>Eric, Sharon & Ken Mahler</h5>
					</div>
				</div>
				<div class='row'>
					<div class='col-xs-3 cardLabel'>
						<h5>Operation Manager:</h5>
					</div>
					<div class='col-xs-7 cardContent'>
						<br>
						<h5>Eric Mahler</h5>
						<h5 class='whiteOut'>(360)697-2004</h5>
						<h5 class='whiteOut'>(360)917-6001</h5>
						<h5><a href="mailto:'eric@caffecocina.com'" class='whiteOut'>eric@caffecocina.com</a></h5>
					</div>
				</div>
				<div class='row'>
					<div class='col-xs-3 cardLabel'>
						<h5>Staff:</h5>
					</div>
					<div class='col-xs-7 cardContent'>
						<h5 class='staffName'>Jordan Sexton</h5><h5 class='staffPosition'>Head Chef</h5>
						<h5 class='staffName'>Chelsea</h5><h5 class='staffPosition'>Lead Barista</h5>
						<h5 class='staffName'>Madison</h5><h5 class='staffPosition'>Barista</h5>	
						<h5 class='staffName'>Paige</h5><h5 class='staffPosition'>Barista</h5>		
						<h5 class='staffName'>Rachel</h5><h5 class='staffPosition'>Barista</h5>	
						<h5 class='staffName'>Sally</h5><h5 class='staffPosition'>Barista</h5>					
					</div>
				</div>
			</div>
		</div>
		<div class='col-xs-9 col-xs-offset-0 col-sm-6  col-sm-offset-3 col-md-4 col-md-offset-1'>

			<div class='contentDiv contentHeight'>
			<!-- Business Card -->			
				<!-- Phone Number -->
				<div class='row'>
					<div class='col-xs-3 cardLabel'>
						<h5>Address:</h5>
					</div>
					<div class='col-xs-8 cardContent'>
						<h5>580 NW Finn Hill Rd</h5>
						<h5>Poulsbo, WA 98370</h5>
					</div>
				</div>
				<div class='row map'>
				<div class='google-maps'>
				<iframe width="280" 
					height="280" 
					
					frameborder="0" 
					style="border:0"
					src="https://www.google.com/maps/embed/v1/place?q=580+NW+Finn+Hill+Rd,+Poulsbo,+WA,+United+States&key=AIzaSyC_96wd3aHaxJO3YefMZr5wiw2vZxOmeq0">
			</iframe>
			</div>
			</div>
			</div>

			

		</div>

			<!-- Right Column -->
			
			
		<div class='col-xs-9 col-xs-offset-0 col-sm-6  col-sm-offset-3 col-md-4 col-md-offset-1'>
			
			<div class='contentDiv'>
				<!-- Business Card -->
				<h5 class='cardInfo'>Social Media</h5>			
					<!-- Phone Number -->
					<div class='col-xs-'>
						<div class='socialMediaIcon'>
							<a href="https://twitter.com/share" data-size="large" data-count="none"><img src='/assets/images/CoffeeCup_Twitter.png'></a>
						</div>	
						<div class='socialMediaIcon'>
							<a href="http://www.facebook.com/poulsbocaffecocina" data-size="large" data-count="none"><img src='/assets/images/CoffeeCup_Facebook.png'></a>
						</div>	
						<div class='socialMediaIcon'>
							<a href="https://instagram.com/caffe_cocina" data-size="large" data-count="none"><img src='/assets/images/CoffeeCup_Instagram.png'></a>
						</div>
						<div class='socialMediaIcon'>
							<a href="http://www.yelp.com/biz/caffe-cocina-poulsbo" data-size="large" data-count="none"><img src='/assets/images/CoffeeCup_Yelp.png'></a>
						</div>	
						<div class='socialMediaIcon'>
							<a href="mailto:'caffecocinacomments@gmail.com" data-size="large" data-count="none"><img src='/assets/images/CoffeeCup_Email.png'></a>
						</div>		
						<div class='socialMediaIcon'>
							<a href="#" data-size="large" data-count="none"><img src='/assets/images/CoffeeCup_GooglePlus.png'></a>
						</div>		
					</div>
			</div>
			
			
		</div>
		<!-- End Right Column -->
		




<!-- END Main body Content -->



</body>
</html>